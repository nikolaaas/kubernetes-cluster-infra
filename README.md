# Kubernetes Cluster Using Terraform and Ansible

The kubernetes cluster is deployed at a local machine running the Proxmox hypervisor. It is a mini High Availability (HA) cluster with 3 master nodes that include etcd, 4 agent nodes (3 normal cpu nodes and 1 gpu node using the nvidia-device-plugin) and an external load balancer (running ha proxy) that handles the API requests to the master nodes. It also utilises MetalLB (Layer 2) to handle trafick to the applications deployed in the agent nodes.

The following schematic demonstrates the layout of the cluster
![image info](kubernetes_layout.jpg)

The scripts developed are quite flexible and can scale to multiple master and agent nodes. The deployment is configured in the "main.tf" file and executed using the following commands:
REMEBER to set a password for your terraform-prov user in proxmox

```bash
$ export PM_PASS="your_proxmox_password"
$ terraform init
$ terraform apply
```

## Requirements
in order to run this setup, one needs to have installed terraform and ansible and helm.

For the gpu node the hardware requirements are:
1) GPUs from NVIDIA
2) Add the nvidia-device-plugin repo for helm
```bash
$ helm repo add nvdp https://nvidia.github.io/k8s-device-plugin
$ helm repo update
```
3) After the terraform apply command exits succesfully then you need to execute:
```bash
$ helm upgrade -i nvdp nvdp/nvidia-device-plugin --namespace nvidia-device-plugin --create-namespace --set gfd.enabled=true
```

In order to delete the plugin one can execute:
```bash
$ helm delete $(helm list | grep nvidia-device-plugin | awk '{print $1}')
```

To test whether the GPU is working properly one can use the following pod manifest named test.yml for example:

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: cuda-vectoradd
spec:
  restartPolicy: OnFailure
  containers:
  - name: cuda-vectoradd
    image: "nvidia/samples:vectoradd-cuda11.2.1"
    resources:
      limits:
         nvidia.com/gpu: 1
```

With:

```bash
kubectl apply -f test.yml
```

And check the logs, which should look something like this:

```bash
$ kubectl logs cuda-vectoradd

Copy input data from the host memory to the CUDA device
CUDA kernel launch with 196 blocks of 256 threads
Copy output data from the CUDA device to the host memory
Test PASSED
Done
```

Install csi-driver-nfs so we can use dynamic volume claims
using a NAS server:
```bash
$ helm repo add csi-driver-nfs https://raw.githubusercontent.com/kubernetes-csi/csi-driver-nfs/master/charts

$ helm install csi-driver-nfs csi-driver-nfs/csi-driver-nfs --namespace kube-system --set controller.replicas=2 
```

To create the storage classes:
```bash
$ kubectl apply -f storage_class.yaml  
```

To test run the following:
```bash
$ kubectl apply -f test-claim.yaml  
$ kubectl apply -f test-pod.yaml
```

To delete:
```bash
helm uninstall csi-driver-nfs -n kube-system
```

To install the k8s dashboard check the README.md in the dashboard directory
