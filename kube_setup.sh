#!/bin/sh

sed -i "s/\(https:\/\/\)\(.*\)\(:6443\)/\1$1\3/g" config
rm -rf ~/.kube/*
cp config ~/.kube/
