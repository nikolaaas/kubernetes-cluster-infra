locals {
  proxmox_ip          = "192.168.88.140"
  ssh_key_pub         = file("~/.ssh/id_rsa.pub") 
  ssh_key             = file("~/.ssh/id_rsa") 
  user                = "nikolas"
  load_balancer_ip    = "192.168.88.10"
  master_nodes        = 3
  master_ip_prefix    = "192.168.88.2%d"
  agent_nodes         = 2 
  agent_ip_prefix     = "192.168.88.3%d"
  gpu_agent_nodes     = 1
  gpu_agent_ip_prefix = "192.168.88.4%d"
  master_ip_lst       = [for i in range(local.master_nodes) : format(local.master_ip_prefix, i)]
  agent_ip_lst        = [for i in range(local.agent_nodes) : format(local.agent_ip_prefix, i)]
  gpu_agent_ip_lst    = [for i in range(local.gpu_agent_nodes) : format(local.gpu_agent_ip_prefix, i)]
  tags                = "PROD,infra"
}

module "vm_provision" {
  source = "./vm_provision"
  proxmox_ip           = local.proxmox_ip
  ssh_key_pub          = local.ssh_key_pub
  user                 = local.user
  load_balancer_ip     = local.load_balancer_ip
  master_nodes         = local.master_nodes
  agent_nodes          = local.agent_nodes
  gpu_agent_nodes      = local.gpu_agent_nodes
  master_ip_lst        = local.master_ip_lst
  agent_ip_lst         = local.agent_ip_lst
  gpu_agent_ip_lst     = local.gpu_agent_ip_lst
  tags                 = local.tags
}

resource "null_resource" "ansible_setup" {
 provisioner "local-exec" {
### sleep for 2 minutes for the machine to cooldown and the vms to properly bootup
  command = "sleep 120 && ansible-playbook vm_provision/ansible/playbooks/setup.yml --user ${local.user} -i vm_provision/ansible/inventory/hosts.ini" 
  }
 depends_on = [module.vm_provision]
}

module "k8s_provision" {
  source = "./k8s_provision"
  ssh_key          = local.ssh_key
  user             = local.user
  load_balancer_ip = local.load_balancer_ip
  master_ip_lst    = local.master_ip_lst
  agent_ip_lst     = concat(local.agent_ip_lst, local.gpu_agent_ip_lst)
  depends_on       = [module.vm_provision, resource.null_resource.ansible_setup]
}

resource "null_resource" "kubectl_setup" {
 provisioner "local-exec" {
### change the kubectl config to have the load balancer ip as the cluser endpoint
  command = "./kube_setup.sh ${local.load_balancer_ip}"
  }
 depends_on = [module.vm_provision,
               resource.null_resource.ansible_setup,
               module.k8s_provision]
}
