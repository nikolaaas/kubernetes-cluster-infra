To install the k8s dashboard:

```bash
$ helm upgrade --install kubernetes-dashboard kubernetes-dashboard/kubernetes-dashboard --create-namespace --namespace kubernetes-dashboard --version 6.0.8
```

create an admin user:
```bash
$ kubectl apply -f dashboard-admin.yaml
```

#create service Ip
```bash
$ kubectl apply -f dashboard-svc.yaml
```

to generate the token
```bash
$ kubectl create token admin-user-dashboard -n kubernetes-dashboard --duration=8760h
```
