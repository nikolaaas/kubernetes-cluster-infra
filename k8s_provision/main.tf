# Configure the RKE provider
#provider "rke" {
#  debug = true
#}

variable "user" {
  type = string
}

variable "ssh_key" {
  type = string
}

variable "master_ip_lst" {
  type = list
}

variable "agent_ip_lst" {
  type = list
}

variable "load_balancer_ip" {
  type = string
}



resource "rke_cluster" "cluster" {
  # Disable port check validation between nodes 
  disable_port_check = false
  enable_cri_dockerd = true

  dynamic "nodes" {
    for_each = var.master_ip_lst 
    content {
      address = nodes.value
      user    = var.user
      role = [
        "controlplane",
        "etcd"
      ]
    ssh_key = var.ssh_key    
    }
  }

  dynamic "nodes" {
    for_each = var.agent_ip_lst 
    content {
      address = nodes.value
      user    = var.user
      role = [
        "worker"
      ]
    ssh_key = var.ssh_key 
    }
  }

  ingress {
    provider = "none"
  }

  network {
    plugin = "flannel"
  }

  authentication {
    strategy = "x509"
    sans     = [var.load_balancer_ip]
  } 

  addons_include = [
    "https://raw.githubusercontent.com/metallb/metallb/v0.13.7/config/manifests/metallb-native.yaml",
    "${path.root}/ip_add_pool.yml",
    "${path.root}/l2_add.yaml"
  ]
}

resource "local_sensitive_file" "kube_config_yaml" {
  filename          = "${path.root}/config"
  content = rke_cluster.cluster.kube_config_yaml
  file_permission   = "0600"
}
