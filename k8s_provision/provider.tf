terraform {
  required_providers {
    rke = {
      source  = "rancher/rke"
      version = "1.5.0"
    }
  }
}
