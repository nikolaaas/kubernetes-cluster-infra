frontend kubernetes-frontend
    bind ${load_balancer_ip}:6443
    mode tcp
    option tcplog
    default_backend kubernetes-backend

backend kubernetes-backend
    mode tcp
    option tcp-check
    balance roundrobin
    %{ for index, addr in master_ip_lst ~}
    server kubernetes-master-${index} ${addr}:6443 check fall 3 rise 2
    %{ endfor ~}
