[proxmox]
${proxmox_ip}
[load_balancer]
${load_balancer_ip}
[master_nodes]
%{ for addr in master_ip_lst ~}
${addr}
%{ endfor ~}
[agent_nodes]
%{ for addr in agent_ip_lst ~}
${addr}
%{ endfor ~}
[gpu_agent_nodes]
%{ for addr in gpu_agent_ip_lst ~}
${addr}
%{ endfor ~}
