terraform {
  required_providers {
    proxmox = {
      source = "Telmate/proxmox"
      version = "3.0.1-rc1"
    }
  }
}

provider "proxmox" {
  pm_parallel       = 2
  pm_tls_insecure   = true
  pm_api_url        = var.pm_api_url
  pm_user           = var.pm_user
  pm_debug = false
}

