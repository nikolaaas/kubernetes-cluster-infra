variable "ssh_key_pub" {
  type = string
}

variable "user" {
  type = string
}

variable "proxmox_ip" {
  type = string
}

variable "load_balancer_ip" {
  type = string
}

variable "master_nodes" {
  type = number
}

variable "agent_nodes" {
  type = number
}

variable "gpu_agent_nodes" {
  type = number
}

variable "master_ip_lst" {
  type = list
}

variable "agent_ip_lst" {
  type = list
}

variable "gpu_agent_ip_lst" {
  type = list
}

variable "tags" {
  type = string
}

resource "local_file" "ansible-inventory" {
 content = templatefile("${path.module}/templates/hosts.tpl",
   {
  proxmox_ip       = var.proxmox_ip
  load_balancer_ip = var.load_balancer_ip
  master_ip_lst    = var.master_ip_lst
  agent_ip_lst     = var.agent_ip_lst
  gpu_agent_ip_lst = var.gpu_agent_ip_lst
   }
 )
 filename = "${path.module}/ansible/inventory/hosts.ini"
}

resource "local_file" "ansible-external-vars" {
 content = templatefile("${path.module}/templates/external_vars.tpl",
   {
  user = var.user
   }
 )
 filename = "${path.module}/ansible/playbooks/external_vars.yml"
}

resource "local_file" "haproxy-cfg" {
 content = templatefile("${path.module}/templates/haproxy.tpl",
   {
  master_ip_lst    = var.master_ip_lst
  load_balancer_ip = var.load_balancer_ip
   }
 )
 filename = "${path.module}/ansible/playbooks/files/haproxy_cfg"
}


resource "proxmox_vm_qemu" "load-balancer" {
  count                  = 1
  name                   = "load-balancer"
  target_node            = "tiny1"
  clone                  = "ubuntu-2204-cloudinit-template"
  os_type                = "cloud-init"
  cores                  = 1
  sockets                = "1"
  cpu                    = "host"
  memory                 = 2048
  balloon                = 1024
  scsihw                 = "virtio-scsi-pci"
  bootdisk               = "scsi0"
  define_connection_info = false
  onboot                 = true
  agent                  = 1
  tags                   = var.tags

  cloudinit_cdrom_storage = "vm_lvm"
  disks {
    scsi {
      scsi0 {
	disk {
	  size     = 10
	  storage  = "vm_lvm"
        }
      }
    }
  }

  network {
    model  = "virtio"
    bridge = "vmbr0"
#    tag    = 100
  }

  lifecycle {
    ignore_changes  = [
      network,
    ]
  }

  # Cloud Init Settings
  ipconfig0 = "ip=${var.load_balancer_ip}/24,gw=192.168.88.1"
  ciuser    = var.user
  sshkeys   = var.ssh_key_pub

}


resource "proxmox_vm_qemu" "k8s-master" {
  count                  = var.master_nodes
  name                   = "k8s-master-${count.index}"
  target_node            = "tiny1"
  clone                  = "ubuntu-2204-cloudinit-template"
  os_type                = "cloud-init"
  cores                  = 1
  sockets                = "1"
  cpu                    = "host"
  memory                 = 8192
  balloon                = 2048
  scsihw                 = "virtio-scsi-pci"
  bootdisk               = "scsi0"
  define_connection_info = false
  onboot                 = true
  agent                  = 1
  tags                   = var.tags

  cloudinit_cdrom_storage = "vm_lvm"
  disks {
    scsi {
      scsi0 {
	disk {
	  size     = 30
	  storage  = "vm_lvm"
        }
      }
    }
  }

  network {
    model  = "virtio"
    bridge = "vmbr0"
#    tag    = 100
  }

  lifecycle {
    ignore_changes  = [
      network,
    ]
  }

  # Cloud Init Settings
  ipconfig0 = "ip=${var.master_ip_lst[count.index]}/24,gw=192.168.88.1"
  ciuser    = var.user
  sshkeys   = var.ssh_key_pub

}


resource "proxmox_vm_qemu" "k8s-agent" {
  count                  = var.agent_nodes 
  name                   = "k8s-agent-${count.index}"
  target_node            = "tiny1"
  clone                  = "ubuntu-2204-cloudinit-template"
  os_type                = "cloud-init"
  cores                  = 2
  sockets                = "1"
  cpu                    = "host"
  memory                 = 10240
  balloon                = 2048
  scsihw                 = "virtio-scsi-pci"
  bootdisk               = "scsi0"
  define_connection_info = false
  onboot                 = true
  agent                  = 1
  tags                   = var.tags

  cloudinit_cdrom_storage = "vm_lvm"
  disks {
    scsi {
      scsi0 {
	disk {
	  size     = 30
	  storage  = "vm_lvm"
        }
      }
    }
  }

  network {
    model  = "virtio"
    bridge = "vmbr0"
#    tag    = 100
  }

  lifecycle {
    ignore_changes  = [
      network,
    ]
  }

  # Cloud Init Settings
  ipconfig0 = "ip=${var.agent_ip_lst[count.index]}/24,gw=192.168.88.1"
  ciuser    = var.user
  sshkeys   = var.ssh_key_pub

}

resource "proxmox_vm_qemu" "agent-gpu" {
  count                  = var.gpu_agent_nodes
  name                   = "k8s-gpu-agent-${count.index}"
  target_node            = "tiny1"
  clone                  = "ubuntu-2204-cloudinit-template"
  os_type                = "cloud-init"
  cores                  = 4
  sockets                = "1"
  cpu                    = "host,hidden=1,flags=+pcid"
  memory                 = 20480
  balloon                = 2048
  scsihw                 = "virtio-scsi-pci"
  bootdisk               = "scsi0"
  define_connection_info = false
  bios                   = "ovmf"
  machine                = "q35"
  onboot                 = true
  agent                  = 1
  tags                   = var.tags

  cloudinit_cdrom_storage = "vm_lvm"
  disks {
    scsi {
      scsi0 {
	disk {
	  size     = 30
	  storage  = "vm_lvm"
        }
      }
    }
  }

  network {
    model  = "virtio"
    bridge = "vmbr0"
#    tag    = 100
  }

  lifecycle {
    ignore_changes  = [
      network,
    ]
  }

  # Cloud Init Settings
  ipconfig0 = "ip=${var.gpu_agent_ip_lst[count.index]}/24,gw=192.168.88.1"
  ciuser    = var.user
  sshkeys   = var.ssh_key_pub

}
